#!/bin/sh

# This script MUST be idempotent.

set -e

configure_net() {
    echo "INFO: enabling etho0"
    cat > /etc/network/interfaces.d/eth0 <<'EOF'
auto eth0
iface eth0 inet dhcp
EOF
    # Minor nitpick: /etc/network/interfaces was group writable
    chmod -R og-w /etc/network
}

get_user_list() {
    for user in $(cd /home && ls); do
        if ! getent passwd "$user" >/dev/null; then
            echo "WARNING: user '$user' is invalid but /home/$user exists" >&2
            continue
        fi
        echo "$user"
    done
    echo "root"
}

configure_ssh() {
    echo "INFO: enabling ssh.service"
    systemctl enable ssh.service

    for user in $(get_user_list | grep -xv root); do
	echo "INFO: configuring ssh for user '$user'"
        sshdir=/home/$user/.ssh
	mkdir -p $sshdir
	cat > $sshdir/authorized_keys <<'EOF'
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC++GHkzHjMiQCehsoJugobXU6TophJtSupig6OwaOqQ7OCxhGTG/h+cct+d0nYbJSOcS9w/2zvH2SACSgEMxLXSFyvz56sABVNYHGKvKcqy9JVa0eHgXbU2e4jQPE3w2iVWKNGYX+uv/tUdf49AokK8AWFpA0YQyzEuinwKoFqFObsXEBX66UCahAqH0t/cRfqETwBrXBaPWb3+BNZpScV9w+VUgX/OC1blYqTj1Kja8dQBw28O5IJzq+PGWi/z0jnslWWlarZAHlzNf0v5gudVnVsRoblDgvprRWDQ4poJqIHnAHRKqywVmQiA2VhxZqvA1CDrWR6n0eq8Xr6b/A9daJI+V8DTSPyomtL8UYMG+aUzNL72Sw3culycw9nKgD5WIlPQzN4mAYJtZKXKWHLJh1C2z7hnQSN8KEtXQntcJLwrZcbrt5yotyCTSW76Ls+L23D6PVploW3tERc9bFOhc4Z55IrrEXw4D+7XnOXRZ9JIioAB2TEdqUw28LpC2I8wNQDJ9jyXNgzEiUhpjEh79ofhz0xhQYj6qLFxvKKk6z63yKhIDFQbRQrVsPLdfpuwt21tzlYftTWsCl3PriPfJBE25wzZHa7/YMPxIaFQyK1aKw20zmJztq4HC8M5ukMDjMktxntVZFCat2bg7tV3FKhKCVOF0HztEKVGvkrbw==
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCjTTU5a8JjCpChH0r8V62Y/Nw513XMHzxWMysbSxuMUG2FDT0ysMX8XS56ikjob4Sh+0AwJXJ4wrP4jO9lSZALAzfCJJ0BcaDYILnliOGQFWaVEMLqaA6WW3fkZJJBPBsQBFJtwx4UKAOPakCuZzsbGJmsGE33P1zB6oyX3VqzWiSmauJGH4cof7QnuQcF3BfPlsu8KwfnAL+ivA0SflyivsxtVs+ycGTmRAwXca30q+EwjFlvFrfgHyv2M9a1bJKgFVLYlZtSf9dAssLUXcKZtCwS1a0muC5vryKtvCDrDNMpBPSzt7NKFuki+drKfJFkCWf5Poa9dTvEUbDr1PXuNE70xqAsDLAD5+0wba/zOiCGoeay8ZaJFQPMimDb+bFam6YJvoMhIjRcblNpuS/nHJsZ2XYq6FPkAovX+nD6Pgs6lj8l/LMLDhRpVkbm8I/a9yPM3bMl/T0qBYHnbtAVtWcC/h9XaqVZu43T4kfJcSqeITgsB98x5pECLbqREKs=
EOF
	chown -R $user.$user $sshdir
	chmod -R og-rwx $sshdir
    done
}

configure_net
configure_ssh
